package com.globalfitness.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;


public class PermissionsActivity {
    // Context context;
    Activity activity;

    public PermissionsActivity(Activity activity) {
        //this.context = context;
        this.activity = activity;

    }

    public static final String TAG = "MainActivity";

    /**
     * Id to identify a camera permission request.
     */
    public static final int REQUEST_CAMERA = 3;

    /**
     * Id to identify a contacts permission request.
     */
    public static final int REQUEST_CONTACTS = 1;

    /**
     * Permissions required to read and write contacts.
     */
    public static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};

    /**
     * Permissions required to read and write contacts.
     */
    private static String[] permissionsCameraGallery = {Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};


    public boolean checkSDCardReadPermission(Context context) {
        return !(ActivityCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
    }

    public void displayCameraAndGalleryPermissionAlert(Activity activity) {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(activity, permissionsCameraGallery, REQUEST_CAMERA);
    }

}
