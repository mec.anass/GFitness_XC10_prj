package com.globalfitness;


import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;

import com.globalfitness.utility.Preferences;



public class SplashScreenActivity extends AppCompatActivity {

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);

        try {
                VideoView videoHolder = (VideoView) this.findViewById(R.id.splash_videoView);
                Uri video = Uri.parse("android.resource://" + getPackageName()
                        + "/" + R.raw.nav_video);
                videoHolder.setVideoURI(video);
                videoHolder.setZOrderOnTop(true);

                videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        public void onCompletion(MediaPlayer mp) {
                                openActivity();
                        }

                });
                videoHolder.start();
        } catch (Exception ex) {
                openActivity();
        }

        }

private void openActivity() {
        if (new Preferences().getUserKey(SplashScreenActivity.this).equals("")) {
        startActivity(new Intent(SplashScreenActivity.this, SignInActivity.class));


        } else {
        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
        }
        finish();
        }



@Override
public void onBackPressed() {
        super.onBackPressed();
        finish();
        }
        }
