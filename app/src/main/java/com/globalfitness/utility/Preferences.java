package com.globalfitness.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.globalfitness.model.UserModel;


public class Preferences {

    private final String USER_KEY = "USER_KEY";
    private final String USER_DETAILS = "USER_DETAILS";
    private final String LOCATION_PERMISSION = "LOCATION_PERMISSION";
    private final String CONTACTS_PERMISSION = "CONTACTS_PERMISSION";
    private final String CAMERA_PERMISSION = "CAMERA_PERMISSION";
    private final String STORAGE_PERMISSION = "STORAGE_PERMISSION";

    //LOcation,Contacts,Camera,Storage

    /**
     * Saves location access permission
     */
    public void saveLocationPermission(Context context, boolean value) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences sp = context.getSharedPreferences(LOCATION_PERMISSION, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("location_permission", value);

        // Commit the edits!
        editor.apply();
    }

    public boolean getLocationPermission(Context context) {
        SharedPreferences customerName = context.getSharedPreferences(LOCATION_PERMISSION, 0);
        return customerName.getBoolean("location_permission", false);
    }




    /**
     * Saves location access permission
     */
    public void saveContactsPermission(Context context, boolean value) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences sp = context.getSharedPreferences(LOCATION_PERMISSION, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("contacts_permission", value);

        // Commit the edits!
        editor.apply();
    }

    public boolean getContactsPermission(Context context) {
        SharedPreferences customerName = context.getSharedPreferences(LOCATION_PERMISSION, 0);
        return customerName.getBoolean("contacts_permission", false);
    }


    /**
     * Saves User Key
     */
    public void saveUserKey(Context context, String value) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences sp = context.getSharedPreferences(USER_KEY, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("Key", value);

        // Commit the edits!
        editor.apply();
    }

    public String getUserKey(Context context) {
        SharedPreferences customerName = context.getSharedPreferences(USER_KEY, 0);
        return customerName.getString("Key", "");
    }

    /**
     * Saves User Id
     */

    public void saveUserDetails(Context context, UserModel userModel) {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences sp = context.getSharedPreferences(USER_DETAILS, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("username", userModel.username);
        editor.putString("email", userModel.email);
        editor.putString("firstName", userModel.firstName);
        editor.putString("lastName", userModel.lastName);
        editor.putString("dob", userModel.dob);
        editor.putString("deviceId", userModel.deviceId);
        editor.putString("imageUrl", userModel.imageUrl);

        // Commit the edits!
        editor.apply();
    }




    public void deleteUserDetails(Context context){
        SharedPreferences sp = context.getSharedPreferences(USER_DETAILS, Activity.MODE_PRIVATE);
        sp.edit().remove("username").apply();
        sp.edit().remove("email").apply();
        sp.edit().remove("firstName").apply();
        sp.edit().remove("lastName").apply();
        sp.edit().remove("dob").apply();
        sp.edit().remove("deviceId").apply();
        sp.edit().remove("imageUrl").apply();

    }


    public String getEmail(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("email", "");

    }

    public String getUserName(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("username", "");

    }


    public String getFirstName(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("firstName", "");

    }

    public String getLastName(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("lastName", "");

    }

    public String getPassword(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("password", "");

    }

    public String getDobirth(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("dob", "");

    }

    public String getImageUrl(Context context) {
        SharedPreferences emialPref = context.getSharedPreferences(USER_DETAILS, 0);
        return emialPref.getString("imageUrl", "");

    }



}
