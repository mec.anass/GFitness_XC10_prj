package com.globalfitness;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.globalfitness.model.UserModel;
import com.globalfitness.utility.Networking;
import com.globalfitness.utility.Preferences;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;



public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private EditText mUserNameEditText, mFirstNameEditText, mLastNameEditText, mEmailEditText;
    private Button mDob_button, mSignUpButton;
    private TextInputEditText mPasswordEditText, mConfirmPasswordEditText;


    //Global variables
    private String dateOfBirth = "";

    //FireBase
    private FirebaseDatabase database;
    private static String USER_REF = "users";
    private static String LOGIN_DETAILS_REF = "login_details";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        database = FirebaseDatabase.getInstance();

        mUserNameEditText = (EditText) findViewById(R.id.userName_editText);
        mPasswordEditText = (TextInputEditText) findViewById(R.id.password_editText);
        mConfirmPasswordEditText = (TextInputEditText) findViewById(R.id.confirm_password_editText);
        mFirstNameEditText = (EditText) findViewById(R.id.first_name_editText);
        mLastNameEditText = (EditText) findViewById(R.id.last_name_editText);
        mEmailEditText = (EditText) findViewById(R.id.email_editText);
        mDob_button = (Button) findViewById(R.id.dob_button);
        mSignUpButton = (Button) findViewById(R.id.signup_button);

        mDob_button.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);

    }

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "dd/MM/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            dateOfBirth = sdf.format(myCalendar.getTime());
            mDob_button.setText(dateOfBirth);

        }

    };

    //error handling
    boolean cancel = false;
    View focusView = null;

    private String password;

    private void signUpNewUser() {

        //EditText data to string
        final String userName = mUserNameEditText.getText().toString().toLowerCase();
        password = mPasswordEditText.getText().toString();
        String confirmPassword = mConfirmPasswordEditText.getText().toString();
        String firstName = mFirstNameEditText.getText().toString();
        String lastName = mLastNameEditText.getText().toString();
        String email = mEmailEditText.getText().toString();

        if (TextUtils.isEmpty(userName)) {

            focusView = mUserNameEditText;
            cancel = true;

            mUserNameEditText.setError(getString(R.string.error_field_required));

        } else if (TextUtils.isEmpty(password)) {

            focusView = mPasswordEditText;
            cancel = true;

            mPasswordEditText.setError(getString(R.string.error_field_required));

        } else if (TextUtils.isEmpty(confirmPassword)) {


            focusView = mConfirmPasswordEditText;
            cancel = true;

            mConfirmPasswordEditText.setError(getString(R.string.error_field_required));

        } else if (password.length() < 4) {

            focusView = mPasswordEditText;
            cancel = true;

            mPasswordEditText.setError(getString(R.string.error_invalid_password));

        } else if (!password.equals(confirmPassword)) {

            focusView = mConfirmPasswordEditText;
            cancel = true;

            mConfirmPasswordEditText.setError(getString(R.string.error_matching_password));

            showSnackBar(getResources().getString(R.string.error_matching_password));

        } else if (TextUtils.isEmpty(firstName)) {
            focusView = mFirstNameEditText;
            cancel = true;

            mFirstNameEditText.setError(getString(R.string.error_field_required));
        } else if (!TextUtils.isEmpty(email) && !isValidEmail(email)) {
            focusView = mEmailEditText;
            cancel = true;

            mEmailEditText.setError(getString(R.string.error_invalid_email));

        } else {
            //Add data
            //Cheking network connections from line 156-169
            if (Networking.isNetworkAvailable(SignUpActivity.this)) {


                UserModel user = new UserModel(userName, email, firstName, lastName,
                        Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID),
                        dateOfBirth, "");

                showProgressDialog();

                //Check weather user already exists and process to signup
                isUserNameExists(user);
            } else {
                showSnackBar(getResources().getString(R.string.no_connection));
               showDialog_singleButton(getResources().getString(R.string.no_connection));
            }


        }

        //shows error if any error occurs
        showError();

    }


    private void showError() {
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
    }

    private void isUserNameExists(final UserModel userModel) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI

                if (dataSnapshot.exists()) {

                    hideProgressDialog();

                /*    String userId = dataSnapshot.getValue().toString();
                    Log.e(getClass().getName(), userId);*/

                    //User exists, notify user to choose different user id
                    focusView = mUserNameEditText;
                    cancel = true;
                    mUserNameEditText.setError(getString(R.string.error_user_name));
                    showSnackBar(getResources().getString(R.string.error_user_name));


                    showError();

                } else {
                    //PAWAN: These below if-else conditions Checks network connections
                    if (Networking.isNetworkAvailable(SignUpActivity.this)) {
                        saveUserDetails(userModel);
                    } else {
                        showSnackBar(getResources().getString(R.string.no_connection));
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                showDialog_singleButton(databaseError.toException().toString());
                // Log.w(getClass().getName(), "loadPost:onCancelled", databaseError.toException());

            }
        };

        DatabaseReference myRef = database.getReference(USER_REF);
        myRef.orderByChild("username").equalTo(userModel.username)
                .addListenerForSingleValueEvent(postListener);
    }

    private void saveUserDetails(final UserModel userModel) {

        hideProgressDialog();
        DatabaseReference myRef = database.getReference(USER_REF);
        final String key = myRef.push().getKey();
        myRef.child(key).setValue(userModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                //Save username and password and assigning key for it
                DatabaseReference myRef = database.getReference(LOGIN_DETAILS_REF);
                myRef.child(userModel.username + "," + password).setValue(key);

                //TODO close previous activity
                new Preferences().saveUserKey(SignUpActivity.this, key);
                new Preferences().saveUserDetails(SignUpActivity.this, userModel);


                Intent i = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(i);
                finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showDialog_singleButton("Failed");
            }
        });
    }

    public static boolean isValidEmail(String email) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.dob_button:

                DatePickerDialog dialog = new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();

                break;

            case R.id.signup_button:


                signUpNewUser();

                break;

            default:
                break;

        }
    }
}








