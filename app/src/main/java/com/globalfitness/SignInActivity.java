package com.globalfitness;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.globalfitness.dialog.DialogFragment_DoubleButton;
import com.globalfitness.interfaces.DialogFragmentInterface;
import com.globalfitness.model.UserModel;
import com.globalfitness.utility.Networking;
import com.globalfitness.utility.Preferences;


import java.util.HashMap;



public class SignInActivity extends BaseActivity implements DialogFragmentInterface {

    //Global variables
    private EditText mUserNameEditText;
    private TextInputEditText mPasswordEditText;
    private Button mSignInButton, mSignUpButton, mSkipButton;

    //FireBase
    private FirebaseDatabase database;

    private static String LOGIN_DETAILS_REF = "login_details";
    private String userKey = "";
    private String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mUserNameEditText = (EditText) findViewById(R.id.userName_editText);
        mPasswordEditText = (TextInputEditText) findViewById(R.id.password_editText);
        mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignUpButton = (Button) findViewById(R.id.sign_up_button);
        mSkipButton = (Button) findViewById(R.id.skip_button);

        //Access FireBase
        database = FirebaseDatabase.getInstance();

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //error handling
                boolean cancel = false;
                View focusView = null;

                String userName = mUserNameEditText.getText().toString().toLowerCase();
                String password = mPasswordEditText.getText().toString();

                if (TextUtils.isEmpty(userName)) {

                    focusView = mUserNameEditText;
                    cancel = true;

                    mUserNameEditText.setError(getString(R.string.error_field_required));
                    showSnackBar(getResources().getString(R.string.error_field_required));

                } else if (TextUtils.isEmpty(password)) {
                    focusView = mPasswordEditText;
                    cancel = true;

                    mPasswordEditText.setError(getString(R.string.error_field_required));
                    showSnackBar(getResources().getString(R.string.error_field_required));

                } else if (password.length() < 4) {
                    focusView = mPasswordEditText;
                    cancel = true;

                    mPasswordEditText.setError(getString(R.string.error_invalid_password));
                    showSnackBar(getResources().getString(R.string.error_invalid_password));

                } else {

                    //: These below if-else conditions is used to Check the network connections
                    if (Networking.isNetworkAvailable(SignInActivity.this)) {

                        showProgressDialog();

                        //Hide virtual keypad after Signin button press
                        InputMethodManager inputManager = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);

                        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);

                        //Process to login
                        login(userName, password);

                    } else {

                        showSnackBar(getResources().getString(R.string.no_connection));
                        showDialog_singleButton(getResources().getString(R.string.no_connection));
                    }

                }

                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }
            }
        });

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
            }
        });

        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    //Get data
    private void login(String username, String password) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI

                if (dataSnapshot.exists()) {
                    userKey = dataSnapshot.getValue().toString();

                    getUserDetails(userKey);

                } else {
                    hideProgressDialog();
                    showDialog_singleButton(getString(R.string.invalid_credentials));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showDialog_singleButton(databaseError.toException().toString());
            }
        };

        DatabaseReference myRef = database.getReference(LOGIN_DETAILS_REF);
        myRef.child(username + "," + password).addListenerForSingleValueEvent(postListener);
    }

    private void getUserDetails(String key) {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI

                deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                hideProgressDialog();

                if (dataSnapshot.exists()) {

                    UserModel userData = dataSnapshot.getValue(UserModel.class);

                    if (!deviceId.equals(userData.deviceId)) {
                        showDialog_doubleButton(getString(R.string.logged_in_other_device));
                    } else {

                        //Save key and user details to preference
                        new Preferences().saveUserKey(SignInActivity.this, userKey);
                        new Preferences().saveUserDetails(SignInActivity.this, userData);

                        //open invite activity

                       /* Here we are sending the message to Invite activity if the sent message is equal to the
                        received message then some actions are performed, read the comment in Invite activity to get
                                a better understanding */

                        Intent i = new Intent(SignInActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    //showDialog_singleButton(getString(R.string.error_fetching_details));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showDialog_singleButton(databaseError.toException().toString());
            }
        };

        DatabaseReference myRef = database.getReference("users");
        myRef.child(key).addListenerForSingleValueEvent(postListener);
    }

    public void showDialog_doubleButton(String message) {
        DialogFragment_DoubleButton newFragment = DialogFragment_DoubleButton.newInstance(
                message);
        newFragment.delegate = SignInActivity.this;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onYesPressed() {
        //Updates deviceId if some other phone its logged in
        HashMap<String, Object> result = new HashMap<>();
        result.put("deviceId", deviceId);
        database.getReference().child("users").child(userKey).updateChildren(result);
        getUserDetails(userKey);

    }
}
