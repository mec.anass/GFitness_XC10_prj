package com.globalfitness.viewholder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.globalfitness.R;
import com.globalfitness.model.WorkoutTypesModel;
import com.squareup.picasso.Picasso;


public class WorkoutDeleteViewHolder extends RecyclerView.ViewHolder {

    public CardView mInviteCardView;
    public TextView workoutsName_textView;
    public ImageView workouts_imageView;
    public RelativeLayout mSingleCardRelativeLayout;
    public Button drop_button;


    public WorkoutDeleteViewHolder(View itemView) {
        super(itemView);

        mInviteCardView = (CardView)itemView.findViewById(R.id.invite_cardView);
        workoutsName_textView = (TextView)itemView.findViewById(R.id.workoutName_textView);
        workouts_imageView = (ImageView)itemView.findViewById(R.id.workouts_imageView);
        mSingleCardRelativeLayout = (RelativeLayout)itemView.findViewById(R.id.singleCardRelativeLayout);
        drop_button = (Button) itemView.findViewById(R.id.drop_button);

    }

    public void bindToWorkoutDelete(Context context, WorkoutTypesModel post, View.OnClickListener relativeLayoutClickListener,
                                    View.OnClickListener dropClickListener
                                   ) {


        workoutsName_textView.setText(post.getName());
        mSingleCardRelativeLayout.setOnClickListener(relativeLayoutClickListener);
        Picasso.with(context).load(post.getUrl()).placeholder(R.drawable.place_holder).fit().centerCrop().into(workouts_imageView);
        drop_button.setOnClickListener(dropClickListener);

    }
}


