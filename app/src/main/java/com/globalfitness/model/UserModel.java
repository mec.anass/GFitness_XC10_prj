package com.globalfitness.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class UserModel {

    public String username;
    public String email;
    public String firstName;
    public String lastName;
    public String dob;
    public String deviceId;
    public String imageUrl;

    public UserModel() {
        // Default constructor required for calls to DataSnapshot.getValue(UserModel.class)
    }

    public UserModel(String username, String email, String firstName, String lastName,
                     String deviceId, String dob, String imageUrl) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.deviceId = deviceId;
        this.imageUrl = imageUrl;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username", username);
        result.put("email", email);
        result.put("firstName", firstName);
        result.put("lastName", lastName);
        result.put("dob", dob);
        result.put("deviceId", deviceId);
        result.put("imageUrl", imageUrl);

        return result;
    }
}
