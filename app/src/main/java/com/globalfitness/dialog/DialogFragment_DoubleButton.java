package com.globalfitness.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.globalfitness.R;
import com.globalfitness.interfaces.DialogFragmentInterface;


public class DialogFragment_DoubleButton extends DialogFragment {

	//Interface
	public DialogFragmentInterface delegate;

	public static DialogFragment_DoubleButton newInstance(String message) {
		DialogFragment_DoubleButton frag = new DialogFragment_DoubleButton();
		Bundle args = new Bundle();
		args.putString("message", message);
		frag.setArguments(args);
		return frag;
	}

	@Override
    @NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("message");

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater

		builder.setMessage(title)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				delegate.onYesPressed();
			}
		}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {

			}
		});      
		return builder.create();
	}

}
