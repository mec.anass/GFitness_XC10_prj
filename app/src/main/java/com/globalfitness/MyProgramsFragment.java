package com.globalfitness;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.globalfitness.model.WorkoutTypesModel;
import com.globalfitness.utility.Preferences;
import com.globalfitness.viewholder.WorkoutDeleteViewHolder;
import com.twotoasters.jazzylistview.recyclerview.JazzyRecyclerViewScrollListener;



@SuppressLint("ValidFragment")
public class MyProgramsFragment extends Fragment {

    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;

    private FirebaseDatabase database;
    private Preferences mPreferences;

    String intentData;

    private FirebaseRecyclerAdapter<WorkoutTypesModel, WorkoutDeleteViewHolder> mAdapter;

    public MyProgramsFragment(String data) {
        // Required empty public constructor

        this.intentData = data;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_satuarday, container, false);

        mRecycler = (RecyclerView) view.findViewById(R.id.invite_frag_recyclerView);
        database = FirebaseDatabase.getInstance();
        mPreferences = new Preferences();

        mRecycler.setHasFixedSize(true);
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        JazzyRecyclerViewScrollListener jazzyScrollListener = new JazzyRecyclerViewScrollListener();
        mRecycler.addOnScrollListener(jazzyScrollListener);

        jazzyScrollListener.setTransitionEffect(11);//animations till 11

        // Inflate the layout for this fragment
        return view;


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getMyConnections();
    }

    private void getMyConnections() {

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("weekdays");
        Query postsQuery = myRef.child(mPreferences.getUserKey(getActivity())).child(intentData);

        mAdapter = new FirebaseRecyclerAdapter<WorkoutTypesModel, WorkoutDeleteViewHolder>
                (WorkoutTypesModel.class,
                        R.layout.item_weekday_delete_workout,
                        WorkoutDeleteViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(WorkoutDeleteViewHolder viewHolder,
                                              final WorkoutTypesModel model, int position) {

                final DatabaseReference postRef = getRef(position);
                final String friend_key = postRef.getKey();

                if (model != null) {
                    viewHolder.bindToWorkoutDelete(getActivity(), model,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    Intent i2 = new Intent(getActivity(), DetailsActivity.class);
                                    i2.putExtra("sampleObject", model);
                                    startActivity(i2);




                                }
                            }, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //Creating the instance of PopupMenu
                                    PopupMenu popup = new PopupMenu(getActivity(), view);
                                    //Inflating the Popup using xml file
                                    popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                                    //registering popup with OnMenuItemClickListener
                                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                        public boolean onMenuItemClick(MenuItem item) {

                                            DatabaseReference my_NotfiRef = database.getReference("weekdays");
                                    my_NotfiRef.child(mPreferences.getUserKey(getActivity())).child(intentData).child(friend_key).removeValue();

                                            return true;
                                        }
                                    });

                                    popup.show();//showing popup menu
                                }
                            });
                }
            }
        };

        mRecycler.setAdapter(mAdapter);
    }



}




