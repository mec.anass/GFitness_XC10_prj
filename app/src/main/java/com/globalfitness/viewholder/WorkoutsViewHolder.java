package com.globalfitness.viewholder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.globalfitness.R;
import com.globalfitness.model.WorkoutTypesModel;
import com.globalfitness.model.WorkoutsModel;
import com.squareup.picasso.Picasso;


public class WorkoutsViewHolder extends ViewHolder {

    public CardView mInviteCardView;
    public TextView workoutsName_textView;
    public ImageView workouts_imageView;
    public RelativeLayout mSingleCardRelativeLayout;

    public WorkoutsViewHolder(View itemView) {
        super(itemView);

        mInviteCardView = (CardView)itemView.findViewById(R.id.invite_cardView);
        workoutsName_textView = (TextView)itemView.findViewById(R.id.workoutName_textView);
        workouts_imageView = (ImageView)itemView.findViewById(R.id.workouts_imageView);
        mSingleCardRelativeLayout = (RelativeLayout)itemView.findViewById(R.id.singleCardRelativeLayout);
    }

    public void bindToWorkouts(Context context, WorkoutsModel post,
                           View.OnClickListener relativeLayoutClickListener) {

        Picasso.with(context).load(post.getUrl()).placeholder(R.drawable.place_holder).fit().centerCrop().into(workouts_imageView);
        workoutsName_textView.setText(post.getName());
        mSingleCardRelativeLayout.setOnClickListener(relativeLayoutClickListener);

    }

    public void bindToWorkoutTypes(Context context, WorkoutTypesModel post,
                                   View.OnClickListener relativeLayoutClickListener) {


        workoutsName_textView.setText(post.getName());
        mSingleCardRelativeLayout.setOnClickListener(relativeLayoutClickListener);
        Picasso.with(context).load(post.getUrl()).placeholder(R.drawable.place_holder).fit().centerCrop().into(workouts_imageView);

    }
}
