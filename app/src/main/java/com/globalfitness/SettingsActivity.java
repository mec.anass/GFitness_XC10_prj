package com.globalfitness;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import com.globalfitness.dialog.PickImageDialog;
import com.globalfitness.interfaces.PickImageDialogInterface;
import com.globalfitness.model.UserModel;
import com.globalfitness.permissions.PermissionsActivity;
import com.globalfitness.utility.Networking;
import com.globalfitness.utility.Preferences;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;



public class SettingsActivity extends BaseActivity implements View.OnClickListener,
        PickImageDialogInterface {

    //Global mutable views
    private ImageView mRemoveImageImageView;
    private ImageView mEditUserImageView;
    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mEmailEditText;
    private Button mDob_button;

    private TextView mToolbarTitleTextView;
    private ProgressDialog mProgressDialog;

    //FireBase
    private FirebaseDatabase database;
    //FireBase storage for saving image
    private StorageReference mStorage;

    //save image uri
    private Uri downloadUri;

    //Global variables
    private String dateOfBirth = "";

    //Preference reference
    private Preferences preferences;

    //Pick image
    private PickImageDialog mPickImageDialog;
    private Uri mImageFileUri;
    private File mImageFile;
    private File mProfileFile;
    private boolean isChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mStorage = FirebaseStorage.getInstance().getReference();

        mProgressDialog = new ProgressDialog(this);

        //Access FireBase
        database = FirebaseDatabase.getInstance();

        configureToolbar();

        RelativeLayout userRelativeLayout = (RelativeLayout) findViewById(R.id.user_relativeLayout);
        mEditUserImageView = (ImageView) findViewById(R.id.edit_user_imageView);
        mRemoveImageImageView = (ImageView) findViewById(R.id.remove_image_imageView);
        ImageView editImageImageView = (ImageView) findViewById(R.id.edit_image_imageView);
        mEmailEditText = (EditText) findViewById(R.id.email_editText);
        mFirstNameEditText = (EditText) findViewById(R.id.first_name_editText);
        mLastNameEditText = (EditText) findViewById(R.id.last_name_editText);
        mDob_button = (Button) findViewById(R.id.dob_button);
        TextView mChangePasswordTextView = (TextView) findViewById(R.id.change_password_textView);

        if (mToolbarTitleTextView != null) {
            //Setting name for toolbar
            mToolbarTitleTextView.setText(getString(R.string.action_settings));
        }

        //creating preference object
        preferences = new Preferences();

        adjustImageViewLayout(userRelativeLayout);

        //Initializing the click listener
        mRemoveImageImageView.setOnClickListener(this);
        editImageImageView.setOnClickListener(this);
        mDob_button.setOnClickListener(this);
        mChangePasswordTextView.setOnClickListener(this);

        //Setting data from preferences to activity
        setUserDetails();

    }

    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mainToolbar != null) {
            mToolbarTitleTextView = (TextView) mainToolbar.findViewById(R.id.toolbar_title_textView);

            setSupportActionBar(mainToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
    }

    private void setUserDetails() {
        if (!preferences.getUserName(this).equals("")) {
            mEmailEditText.setText(preferences.getEmail(this));
            mFirstNameEditText.setText(preferences.getFirstName(this));
            mLastNameEditText.setText(preferences.getLastName(this));
            downloadUri = Uri.parse(preferences.getImageUrl(this));

            if (downloadUri != null && !downloadUri.equals(Uri.EMPTY)) {
                Picasso.with(SettingsActivity.this)
                        .load(downloadUri)
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.user_profile_place_holder)
                        .into(mEditUserImageView);

            }

            String dob = preferences.getDobirth(this);
            if (!TextUtils.isEmpty(dob)) {
                mDob_button.setText(dob);
            } else {
                mDob_button.setText(getString(R.string.dob));
            }
        }
    }

    private void saveUserImage() {
        mProgressDialog.setMessage("Uploading");
        mProgressDialog.show();

        Uri file = Uri.fromFile(mProfileFile);


        //  Here he ware uploadiing the images under "photos" directory with same name i.e., the username that comes with login
        StorageReference filepath = mStorage.child("Photos").child(new Preferences().getUserName(getApplicationContext()));

        filepath.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                mProgressDialog.dismiss();
                @SuppressWarnings("VisibleForTests") Uri imageUri = taskSnapshot.getDownloadUrl();
                downloadUri = imageUri;

                saveDetails();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.dismiss();
                Toast.makeText(SettingsActivity.this, "Image Upload failed", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void saveDetails() {
        //Add data
        UserModel user = new UserModel(preferences.getUserName(getApplicationContext()), email,
                firstName, lastName, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID),
                dateOfBirth, downloadUri.toString());

        new Preferences().saveUserDetails(SettingsActivity.this, user);

        //Updates user details
        Map<String, Object> postValues = user.toMap();

        String key = new Preferences().getUserKey(getApplicationContext());
        database.getReference().child("users").child(key).updateChildren(postValues);

        showSnackBar(getResources().getString(R.string.update_success));
    }

    //Adjust image view layout as per 16:9 ratio, so as keep the aspect ratio
    private void adjustImageViewLayout(RelativeLayout userRelativeLayout) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        LinearLayout.LayoutParams relBtn = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, (int) Math.round(height * (0.35)));
        userRelativeLayout.setLayoutParams(relBtn);

    }

    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "dd/MM/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            dateOfBirth = sdf.format(myCalendar.getTime());
            mDob_button.setText(dateOfBirth);

        }

    };

    private void checkGalleryPermission() {

        if (Build.VERSION.SDK_INT >= 23) {

            if (!(new PermissionsActivity(this).checkSDCardReadPermission(this))) {
                new PermissionsActivity(this).displayCameraAndGalleryPermissionAlert(this);

            } else {
                mPickImageDialog.showDialog();
            }
        } else {
            mPickImageDialog.showDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    private String email, firstName, lastName;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_done:

                hideSoftKeyboard(mEmailEditText, this);
                hideSoftKeyboard(mFirstNameEditText, this);
                hideSoftKeyboard(mLastNameEditText, this);

                if (Networking.isNetworkAvailable(SettingsActivity.this)) {
                    // Reset errors.
                    mEmailEditText.setError(null);
                    mFirstNameEditText.setError(null);
                    mLastNameEditText.setError(null);

                    email = mEmailEditText.getText().toString();
                    firstName = mFirstNameEditText.getText().toString();
                    lastName = mLastNameEditText.getText().toString();

                    boolean cancel = false;
                    View focusView = null;

                    if (TextUtils.isEmpty(firstName)) {


                        focusView = mFirstNameEditText;
                        cancel = true;

                        mFirstNameEditText.setError(getString(R.string.error_field_required));

                    } else if (!isValidEmail(email)) {

                        focusView = mEmailEditText;
                        cancel = true;

                        mEmailEditText.setError(getString(R.string.error_invalid_email));

                        showSnackBar(getResources().getString(R.string.error_invalid_email));

                    } else {

                        if (isChanged) {
                            saveUserImage();
                        } else {
                            saveDetails();
                        }
                    }
                    if (cancel) {
                        // There was an error; don't attempt login and focus the first
                        // form field with an error.
                        focusView.requestFocus();
                    }
                } else {
                    showSnackBar(getResources().getString(R.string.no_connection));
                    showDialog_singleButton(getResources().getString(R.string.no_connection));
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.dob_button:

                DatePickerDialog dialog = new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();

                break;

            case R.id.remove_image_imageView:
                mProfileFile = null;

                mRemoveImageImageView.setVisibility(View.GONE);

                mEditUserImageView.setImageResource(R.drawable.user_profile_place_holder);

                break;

            case R.id.edit_image_imageView:
                if (mPickImageDialog == null) {
                    mPickImageDialog = new PickImageDialog(this);
                    mPickImageDialog.delegate = this;
                }

                checkGalleryPermission();

                break;

            case R.id.change_password_textView:
                if (Networking.isNetworkAvailable(SettingsActivity.this)) {

                    startActivity(new Intent(this, ChangePasswordActivity.class));
                } else {
                    showSnackBar(getResources().getString(R.string.no_connection));
                    showDialog_singleButton(getResources().getString(R.string.no_connection));
                }
                break;

            default:
                break;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions,
                                           @Nullable int[] grantResults) {
        switch (requestCode) {
            case PermissionsActivity.REQUEST_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults != null && grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    mPickImageDialog.showDialog();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mPickImageDialog == null) {
            mPickImageDialog = new PickImageDialog(this);
            mPickImageDialog.delegate = this;
            mPickImageDialog.resetFiles(mImageFileUri, mImageFile);
        }

        if (resultCode == Activity.RESULT_OK) {
            mPickImageDialog.onActivityResult(requestCode, data);

        } else {
            mPickImageDialog.onResultCancelled();
        }
    }

    public static boolean isValidEmail(String email) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


    @Override
    public void holdRecordingFile(Uri fileUri, File file) {
        this.mImageFileUri = fileUri;
        this.mImageFile = file;
    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void displayPickedImage(File file) {
        mProfileFile = file;
        isChanged = true;

        Picasso.with(this).load(file)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.user_profile_place_holder)
                .into(mEditUserImageView);

        mRemoveImageImageView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(MainActivity.EDIT_USER_CALLBACK);
    }
}



