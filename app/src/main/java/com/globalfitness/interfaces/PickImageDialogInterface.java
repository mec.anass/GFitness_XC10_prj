package com.globalfitness.interfaces;

import android.content.Intent;
import android.net.Uri;

import java.io.File;

public interface PickImageDialogInterface {

    void holdRecordingFile(Uri fileUri, File file);
    void handleIntent(Intent intent, int requestCode);
    void displayPickedImage(File file);

}
