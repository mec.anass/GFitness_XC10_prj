package com.globalfitness;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.globalfitness.dialog.DialogFragment_SingleButton;


public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    public void showDialog_singleButton(String message) {
        DialogFragment_SingleButton newFragment = DialogFragment_SingleButton.newInstance(
                message);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Shows snack bar with the message supplied
     *
     * @param message Message to be displayed
     */
    public void showSnackBar(String message) {
        try {
            Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), message,
                    Snackbar.LENGTH_SHORT);

            //Set text color to white
            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);

            //Show the snack bar
            snackbar.show();
        } catch (Exception e) {
            //Do Nothing
        }

    }

    public void hideSoftKeyboard(EditText editText, Context context) {
        try {
            InputMethodManager imm = (InputMethodManager)context.
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        } catch (Exception e){
            //DO Nothing
        }


    }


}
