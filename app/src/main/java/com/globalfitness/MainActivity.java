package com.globalfitness;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.globalfitness.model.WorkoutsModel;
import com.globalfitness.utility.Preferences;
import com.globalfitness.viewholder.WorkoutsViewHolder;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;
import com.twotoasters.jazzylistview.recyclerview.JazzyRecyclerViewScrollListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView mToolbarTitleTextView;
    private RecyclerView mRecycler;
    private TextView mNavUserNameTextView, mNavUserEmailTextView;
    private Preferences mPreferences;
    private CircleImageView mProfileImage;
    private ProgressDialog progressDialog;
    private NavigationView navigationView;

    Toolbar mainToolbar;

    private FirebaseDatabase database;


    private FirebaseRecyclerAdapter<WorkoutsModel, WorkoutsViewHolder> mAdapter;
    public static final int EDIT_USER_CALLBACK = 1003;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();

        configureToolbar();

        mRecycler = (RecyclerView) findViewById(R.id.invite_recyclerView);
        //NavigationView
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        mNavUserNameTextView = (TextView) header.findViewById(R.id.user_name_textView);
        mNavUserEmailTextView = (TextView) header.findViewById(R.id.user_email_textView);
        mProfileImage = (CircleImageView) header.findViewById(R.id.profile_image);

        mRecycler.setHasFixedSize(true);
        LinearLayoutManager  mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);


        JazzyRecyclerViewScrollListener jazzyScrollListener = new JazzyRecyclerViewScrollListener();
        mRecycler.addOnScrollListener(jazzyScrollListener);

        //SET YOUR ANIMATIONS here till 11
        jazzyScrollListener.setTransitionEffect(1);

        //Progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));

        //Creating Preferences object
        mPreferences = new Preferences();

        showUserDetails();

        if (mToolbarTitleTextView != null) {
            //Setting name for toolbar
            mToolbarTitleTextView.setText("Workouts");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        // find MenuItem you want to change
        MenuItem nav_camara = menu.findItem(R.id.nav_logout);

        // set new title to the MenuItem
        if (new Preferences().getUserKey(this).equals("")){
            nav_camara.setTitle("Sign in");
        } else {
            nav_camara.setTitle("Logout");

        }
        getWorkouts();

    }

    private void configureToolbar() {
        mainToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mainToolbar != null) {
            mToolbarTitleTextView = (TextView) mainToolbar.findViewById(R.id.toolbar_title_textView);
            mToolbarTitleTextView.setText(getString(R.string.app_name));
            setSupportActionBar(mainToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

    }

    private void showUserDetails() {
        //Assigning username and email to NavigationView text

        if (!mPreferences.getFirstName(this).equals("")){
            mNavUserNameTextView.setText(mPreferences.getFirstName(this));
            mNavUserEmailTextView.setText(mPreferences.getEmail(this));
            if (!mPreferences.getImageUrl(this).equals("")) {
                Picasso.with(this).load(mPreferences.getImageUrl(this))
                        .placeholder(R.drawable.ic_action_account_circle_40)
                        .fit().centerCrop().into(mProfileImage);

            }
        }

    }


    private void getWorkouts() {

        DatabaseReference myRef = database.getReference("workouts");
        Query postsQuery = myRef;
        mAdapter = new FirebaseRecyclerAdapter<WorkoutsModel, WorkoutsViewHolder>
                (WorkoutsModel.class,
                        R.layout.item_large_workouts,
                        WorkoutsViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(WorkoutsViewHolder viewHolder,
                                              final WorkoutsModel model, int position) {

                if (model != null) {
                    viewHolder.bindToWorkouts(MainActivity.this, model,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    Intent i = new Intent(MainActivity.this, WorkoutTypesActivity.class);
                                    i.putExtra("from_workouts", model.getName());

                                    startActivity(i);

                                }
                            });
                }
            }
        };

        mRecycler.setAdapter(mAdapter);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    boolean close = true;
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_programs) {
            //Handle the gallery action

            if (!new Preferences().getUserKey(this).equals("")){
                Intent i = new Intent(MainActivity.this, ProgramsActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(i);
            }

        }
        else if (id == R.id.nav_bmi) {

            Intent i = new Intent(MainActivity.this, BmiActivity.class);
            startActivity(i);

        }

        else if (id == R.id.nav_settings) {

            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);

        }

        else if (id == R.id.nav_logout) {
            close = true;

            if (!new Preferences().getUserKey(this).equals("")) {

                new Preferences().saveUserKey(this, "");
                new Preferences().deleteUserDetails(this);
                Intent loginIntent = new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(loginIntent);
                finish();

            }else {
                Intent i = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(i);

            }

        } else if (id == R.id.nav_share) {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Here is the share content body";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } else if (id == R.id.nav_send) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
