package com.globalfitness;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.globalfitness.interfaces.WeekdayDialogInterface;
import com.globalfitness.model.WorkoutTypesModel;
import com.globalfitness.utility.Preferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class DetailsActivity extends BaseActivity implements WeekdayDialogInterface{


    private TextView mToolbarTitleTextView;
    private FirebaseDatabase database;
    //private String randomKey;

    WorkoutTypesModel sample;
    private Preferences mPreferences;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detials);

        database = FirebaseDatabase.getInstance();


        Intent i = getIntent();
        sample = (WorkoutTypesModel) i.getSerializableExtra("sampleObject");

        final VideoView videoView = (VideoView) findViewById(R.id.VideoView);
        TextView mNameTextView = (TextView) findViewById(R.id.nameTextView);
        TextView description = (TextView) findViewById(R.id.descriptionTextView);
        ImageButton fullscreen = (ImageButton) findViewById(R.id.play_button);
        mPreferences = new Preferences();

        configureToolbar();

        if (mToolbarTitleTextView != null) {
            //Setting name for toolbar
            mToolbarTitleTextView.setText(sample.getName());
        }

        mNameTextView.setText(sample.getName());

        fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(DetailsActivity.this, FullscreenVideoActivity.class);
                i.putExtra("from_workouts_types", sample.getVidurl());

                //i.putExtra("from_workouts_types_description", sample.getDescription());
                startActivity(i);
            }
        });
        //These three lines will give you the media controllers on you video
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.setVideoPath(sample.getVidurl());

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        videoView.start();

        description.setText(sample.getDescription());


    }

    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mainToolbar != null) {
            mToolbarTitleTextView = (TextView) mainToolbar.findViewById(R.id.toolbar_title_textView);

            setSupportActionBar(mainToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_addto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_add:


                WeekdayDialogFragment newFragment = WeekdayDialogFragment.newInstance("");
                newFragment.delegate = DetailsActivity.this;
                newFragment.show(getSupportFragmentManager(), "dialog");

                return true;

            case R.id.action_settings:

                Intent i = new Intent(DetailsActivity.this, ProgramsActivity.class);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }


    }


    @Override
    public void onYesPressed(ArrayList<String> result) {


        if (!new Preferences().getUserKey(this).equals("")){
            DatabaseReference myRef = database.getReference("weekdays");


            for (int i=0; i<result.size(); i++){

                final String randomKey = myRef.push().getKey();

                myRef.child(mPreferences.getUserKey(this)).child(result.get(i)).child(randomKey).setValue(sample);
            }
        } else {
            showDialog_singleButton("Please sign in to add workout");
        }
    }
}





