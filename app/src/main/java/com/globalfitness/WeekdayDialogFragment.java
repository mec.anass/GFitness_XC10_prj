package com.globalfitness;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.google.firebase.database.FirebaseDatabase;
import com.globalfitness.interfaces.WeekdayDialogInterface;
import com.globalfitness.model.WorkoutTypesModel;

import java.util.ArrayList;



public class WeekdayDialogFragment extends DialogFragment {
    public WeekdayDialogInterface delegate;

    static String[] weekdays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Satuarday"};


    public static WeekdayDialogFragment newInstance(String message) {
        WeekdayDialogFragment frag = new WeekdayDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ArrayList<String> mSelectedItems = new ArrayList();  // Where we track the selected items
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        builder.setTitle(R.string.dialog_name)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setMultiChoiceItems(R.array.pick_days, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {

                                    // If the user checked the item, add it to the selected items
                                    //This line adds the selected workout to the selected day
                                    mSelectedItems.add(weekdays[which]);
                                }

                                else {
                                    mSelectedItems.remove(weekdays[which]);
                                }
                            }
                        })
                // Set the action buttons
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK, so save the mSelectedItems results somewhere
                        // or return them to the component that opened the dialog


                        //Intent returnIntent = new Intent();
                        delegate.onYesPressed(mSelectedItems);


                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });


        return builder.create();
    }

}


