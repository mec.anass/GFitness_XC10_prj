package com.globalfitness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.globalfitness.interfaces.DialogFragmentInterface;
import com.globalfitness.model.WorkoutTypesModel;
import com.globalfitness.viewholder.WorkoutsViewHolder;
import com.twotoasters.jazzylistview.recyclerview.JazzyRecyclerViewScrollListener;


public class WorkoutTypesActivity extends BaseActivity {

    private TextView mToolbarTitleTextView;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private RelativeLayout mRelativeLayout;
    public DialogFragmentInterface delegate;

    private FirebaseDatabase database;


    private FirebaseRecyclerAdapter<WorkoutTypesModel, WorkoutsViewHolder> mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);

        database = FirebaseDatabase.getInstance();

        configureToolbar();

        mRecycler = (RecyclerView) findViewById(R.id.invite_recyclerView);

        mRecycler.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        JazzyRecyclerViewScrollListener jazzyScrollListener = new JazzyRecyclerViewScrollListener();
        mRecycler.addOnScrollListener(jazzyScrollListener);
        jazzyScrollListener.setTransitionEffect(2);

        if (mToolbarTitleTextView != null) {
            //Setting name for toolbar
            mToolbarTitleTextView.setText("Workout Types");
        }


        Bundle bundle = getIntent().getExtras();
        String message = bundle.getString("from_workouts");

        getMyConnections(message);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }


    }

    private void configureToolbar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mainToolbar != null) {
            mToolbarTitleTextView = (TextView) mainToolbar.findViewById(R.id.toolbar_title_textView);

            setSupportActionBar(mainToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }

    }



    private void getMyConnections(String s) {

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("workout_types");
        Query postsQuery = myRef.child(s);
        mAdapter = new FirebaseRecyclerAdapter<WorkoutTypesModel, WorkoutsViewHolder>
                (WorkoutTypesModel.class,
                        R.layout.item_large_workouts,
                        WorkoutsViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(WorkoutsViewHolder viewHolder,
                                              final WorkoutTypesModel model, int position) {

                if (model != null) {
                    viewHolder.bindToWorkoutTypes(WorkoutTypesActivity.this, model,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                    Intent i2 = new Intent(WorkoutTypesActivity.this, DetailsActivity.class);
                                    i2.putExtra("sampleObject", model);
                                    startActivity(i2);


                                }
                            });
                }
            }
        };

        mRecycler.setAdapter(mAdapter);
    }



}

